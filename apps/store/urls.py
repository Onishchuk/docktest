
from django.urls import path

from apps.store.views import *


urlpatterns = [

    path('<int:orderId>/', OrderViews.as_view()),

]