# Generated by Django 2.2.7 on 2019-12-01 14:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('pet', '0004_auto_20191201_1436'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('quantity', models.IntegerField(default=0)),
                ('shipDate', models.DateTimeField()),
                ('status', models.CharField(choices=[('p', 'placed'), ('a', 'approved'), ('d', 'delivered')], max_length=100)),
                ('complete', models.BooleanField(default=True)),
                ('petId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pet.Pet')),
            ],
        ),
    ]
