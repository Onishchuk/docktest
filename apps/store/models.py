import enum

from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

from apps.pet.models import Pet

User = get_user_model()


class Order(models.Model):
    name = models.CharField(max_length=200)
    petId = models.ForeignKey(Pet, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    shipDate = models.DateTimeField()
    STATUS_TYPE_CHOICES = (
        ('p', 'placed'),
        ('a', 'approved'),
        ('d', 'delivered'),
    )
    status = models.CharField(max_length=100, choices=STATUS_TYPE_CHOICES)
    complete = models.BooleanField(default=True)

    def __str__(self):
        return unicode(self.name)
