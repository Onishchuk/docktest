
from django.contrib import admin

from apps.pet.models import *


admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Pet)
admin.site.register(PhotoPet)
