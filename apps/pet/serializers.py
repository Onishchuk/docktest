from rest_framework import serializers

from apps.pet.models import Pet, Category, Tag, PhotoPet

from rest_framework import serializers

class ChoicesField(serializers.Field):
    def __init__(self, choices, **kwargs):
        self._choices = choices
        super(ChoicesField, self).__init__(**kwargs)

    def to_representation(self, obj):
        return self._choices[obj]

    def to_internal_value(self, data):
        return getattr(self._choices, data)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class PetSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)
    tags = TagSerializer(read_only=True, many=True)

    class Meta:
        model = Pet
        fields = '__all__'


class SmallPetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pet
        fields = ['name', 'status']


class PhotoPetSerializer(serializers.ModelSerializer):

    class Meta:
        model = PhotoPet
        fields = '__all__'
