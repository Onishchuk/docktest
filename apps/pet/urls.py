
from django.contrib import admin
from django.urls import path, include

from apps.pet.views import *

urlpatterns = [

    path('', PetViews.as_view()),

    path('FindByStatusPetViews/', FindByStatusPetViews.as_view()),

    path('<int:petId>/', SinglePetViews.as_view()),

    path('uploadImage/<int:petId>', UploadImagePetViews.as_view()),

]