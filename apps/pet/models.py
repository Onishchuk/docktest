import enum

from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

User = get_user_model()


class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return unicode(self.name)


class Tag(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return unicode(self.name)


class Pet(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200)
    photoUrls = models.CharField(max_length=200)
    tags = models.ManyToManyField(Tag)
    STATUS_TYPE_CHOICES = (
        ('available', 'available'),
        ('pending', 'pending'),
        ('sold', 'sold'),
    )
    status = models.CharField(max_length=100, choices=STATUS_TYPE_CHOICES)

    def __str__(self):
        return unicode(self.name)


class PhotoPet(models.Model):
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    additionalMetadata = models.CharField(max_length=200)
    file = models.ImageField()

    def __str__(self):
        return unicode(self.additionalMetadata)
