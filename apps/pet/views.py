
from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.pet import models
from apps.pet.models import Pet
from apps.pet.serializers import PetSerializer, SmallPetSerializer, PhotoPetSerializer

User = get_user_model()


class PetViews(APIView):

    def post(self, request, format=None):
        serializer = PetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def put(self, request, format=None):
    #     serializer = PetSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FindByStatusPetViews(generics.ListAPIView):
    serializer_class = PetSerializer

    def get_queryset(self):
        status = self.request.query_params.get('status', None)
        return Pet.objects.filter(status=status)


class SinglePetViews(APIView):

    def get_object(self, pk):
        try:
            return Pet.objects.get(pk=pk)
        except Pet.DoesNotExist:
            raise Http404

    def get(self, request, petId, format=None):
        snippet = self.get_object(petId)
        serializer = PetSerializer(snippet)
        return Response(serializer.data)

    def post(self, request, petId, format=None):
        snippet = self.get_object(petId)
        serializer = SmallPetSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PetSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UploadImagePetViews(APIView):

    def post(self, request, petId=None, format=None):
        serializer = PhotoPetSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)