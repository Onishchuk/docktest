from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    phone = models.CharField(max_length=160, null=True, blank=True)
    userStatus = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.username
