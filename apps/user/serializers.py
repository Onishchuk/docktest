
from rest_framework import serializers

from apps.store.models import Order, User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password', 'phone', 'userStatus', ]
