
from django.urls import path

from apps.user.views import UserViews, CreateViews, CreateWithArrayViews, CreateWithListViews

urlpatterns = [

    path('', CreateViews.as_view()),
    path('CreateWithArray/', CreateWithArrayViews.as_view()),
    path('CreateWithList/', CreateWithListViews.as_view()),
    path('<str:username>/', UserViews.as_view()),

]